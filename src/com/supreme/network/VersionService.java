package com.supreme.network;

import com.supreme.models.PaginationData;
import com.supreme.models.Version;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.jetbrains.annotations.NotNull;
import retrofit2.Response;
import retrofit2.http.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface VersionService {


    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"
    })
    @GET("/api/v1/version")
    Observable<YoResponse<ArrayList<Version>, PaginationData>> search(@QueryMap HashMap<String, Object> map);

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$",
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36",
            "referer: https://yw-web.yoworld.com/",
            "x-requested-with: ShockwaveFlash"
    })
    @GET("/api/v1/version/{md5}")
    Observable<Response<ResponseBody>> get(@Path("md5") String md5);

    @Multipart
    @Headers({"Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @POST("/api/v1/version")
    Observable<YoResponse<Version, PaginationData>> store(
            @Part("description") RequestBody description,
            @Part MultipartBody.Part file,
            @Part("md5") RequestBody md5,
            @Part("codename") RequestBody codename,
            @Part("version") RequestBody version,
            @Part("size") RequestBody size,
            @Part("created_at") RequestBody created_at
    );

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @PATCH("/api/v1/version/{md5}")
    Observable<YoResponse<Version, PaginationData>> update(
            @Path("md5") String md5,
            @Body HashMap<String, Object> map);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @DELETE("/api/v1/version/{md5}")
    Observable<YoResponse<String, PaginationData>> delete(@Path("md5") String md5);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @POST("/api/v1/update/{yo_tech_dig}/{md5}")
    Observable<Response<ResponseBody>> setTechDigHash(@NotNull @Path("yo_tech_dig") String yo_tech_dig, @Path("md5") @NotNull String md5);
}
