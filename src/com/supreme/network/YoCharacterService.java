package com.supreme.network;

import com.supreme.models.PaginationData;
import com.supreme.models.YoCharacter;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface YoCharacterService {


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @GET("/api/v1/characters")
    Observable<YoResponse<ArrayList<YoCharacter>, PaginationData>> search(@QueryMap HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @PATCH("/api/v1/characters/{id}")
    Observable<YoResponse<YoCharacter, PaginationData>> update(@Path("id") int id, @Body HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @DELETE("/api/v1/characters/{id}")
    Observable<YoResponse<YoCharacter, PaginationData>> delete(@Path("id") int id);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @GET("/api/v1/characters/{id}")
    Observable<YoResponse<YoCharacter, PaginationData>> get(@Path("id") int id);


}
