package com.supreme.network

class Meta {
    var current_page = 0
    var from = 0
    var last_page = 0
    var path: String? = null
    var per_page = 0
    var to = 0
    var total = 0
}