package com.supreme.network

class Link {
    var first: String? = null
    var last: String? = null
    var prev: String? = null
    var next: String? = null
}