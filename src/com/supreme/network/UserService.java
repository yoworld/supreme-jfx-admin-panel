package com.supreme.network;

import com.supreme.models.PaginationData;
import com.supreme.models.User;
import com.supreme.models.YoAuth;
import com.supreme.models.YoSettings;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface UserService {
    @Headers({
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"
    })
    @GET("/api/v1/user")
    Observable<YoResponse<ArrayList<User>, PaginationData>> search(@QueryMap HashMap<String, Object> query);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @PATCH("/api/v1/user/{id}")
    Observable<YoResponse<User, PaginationData>> update(@Path("id") int id, @Body HashMap<String, Object> query);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @DELETE("/api/v1/user/{id}")
    Observable<YoResponse<User, PaginationData>> delete(@Path("id") int id);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @GET("/api/v1/user/{id}")
    Observable<YoResponse<User, PaginationData>> get(@Path("id") int id);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @GET("/api/v1/user/{id}/active")
    Observable<YoResponse<ArrayList<YoAuth>, PaginationData>> getActiveAccounts(@Path("id") int id);
}
