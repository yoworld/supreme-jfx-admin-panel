package com.supreme.network

class YoResponse<D, E> {
    var status = false
    var message: String? = null
    var data: D? = null
    var extra: E? = null
    var meta: Meta? = null
    var links: Link? = null
}