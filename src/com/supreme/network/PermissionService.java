package com.supreme.network;

import com.supreme.models.PaginationData;
import com.supreme.models.Permission;
import com.supreme.models.User;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface PermissionService {

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @GET("/api/v1/user/{id}/permission")
    Observable<YoResponse<ArrayList<Permission>, PaginationData>> search(@Path("id") int id, @QueryMap HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @POST("/api/v1/user/{id}/permission")
    Observable<YoResponse<Permission, PaginationData>> store(@Path("id") int id, @Body HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @PATCH("/api/v1/user/{id}/permission/{hash}")
    Observable<YoResponse<Permission, PaginationData>> update(@Path("id") int id, @Path("hash") String hash, @Body HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @DELETE("/api/v1/user/{id}/permission/{hash}")
    Observable<YoResponse<Permission, PaginationData>> delete(@Path("id") int id, @Path("hash") String hash);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @GET("/api/v1/user/{id}/permission/{hash}")
    Observable<YoResponse<Permission, PaginationData>> get(@Path("id") int id, @Path("hash") String hash);

}
