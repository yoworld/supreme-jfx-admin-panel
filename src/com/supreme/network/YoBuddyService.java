package com.supreme.network;

import com.supreme.models.PaginationData;
import com.supreme.models.YoBuddy;
import com.supreme.models.YoCharacter;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface YoBuddyService {


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @GET("/api/v1/supreme/{id}/buddy")
    Observable<YoResponse<ArrayList<YoBuddy>, PaginationData>> search(@Path("id") int id, @QueryMap HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @PATCH("/api/v1/supreme/{user_id}/buddy/{buddy_id}")
    Observable<YoResponse<YoBuddy, PaginationData>> update(@Path("user_id") int id, @Path("buddy_id") int buddy_id, @Body HashMap<String, Object> query);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @DELETE("/api/v1/supreme/{user_id}/buddy/{buddy_id}")
    Observable<YoResponse<YoBuddy, PaginationData>> delete(@Path("user_id") int user_id, @Path("buddy_id") int buddy_id);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou*"})
    @GET("/api/v1/supreme/{user_id}/buddy/{buddy_id}")
    Observable<YoResponse<YoBuddy, PaginationData>> get(@Path("user_id") int user_id, @Path("buddy_id") int buddy_id);

}
