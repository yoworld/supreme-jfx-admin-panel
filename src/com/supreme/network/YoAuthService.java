package com.supreme.network;

import com.supreme.models.PaginationData;
import com.supreme.models.YoAuth;
import com.supreme.models.YoSettings;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface YoAuthService {


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @GET("/api/v1/supreme")
    Observable<YoResponse<ArrayList<YoAuth>, PaginationData>> search(@QueryMap HashMap<String, Object> query);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @PATCH("/api/v1/supreme/{id}")
    Observable<YoResponse<YoAuth, PaginationData>> update(@Path("id") int id, @Body HashMap<String, Object> query);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @PATCH("/api/v1/settings/{id}?decode=true")
    Observable<YoResponse<YoSettings, PaginationData>> settings(@Path("id") int id, @Body HashMap<String, Object> query);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @DELETE("/api/v1/supreme/{id}")
    Observable<YoResponse<YoAuth, PaginationData>> delete(@Path("id") int id);


    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @GET("/api/v1/supreme/{id}")
    Observable<YoResponse<YoAuth, PaginationData>> get(@Path("id") int id);

    @Headers({"Content-Type: application/json", "Accept: application/json",
            "Authorization: Bearer BigBrotherIsWatchingYou$"})
    @POST("/api/v1/supreme")
    Observable<YoResponse<YoAuth, PaginationData>> create(@Body String body);
}
