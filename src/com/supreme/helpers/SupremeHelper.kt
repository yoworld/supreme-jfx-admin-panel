package com.supreme.helpers

import javafx.scene.control.TableView
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import java.io.*
import java.nio.ByteBuffer
import java.nio.charset.CharacterCodingException
import java.nio.charset.Charset
import java.util.*
import java.util.regex.Pattern

object SupremeHelper {

    var message = "I like communism."
    var end = "հԙՕՐՒ՜ԙ՚ՖՔՔՌ\u0557ՐՊՔԗ"
    val data: String?
        get() {

            try {
                val inputStream = FileInputStream(File("/Users/admin/Desktop/IdeaProjects/SupremeTeam/Shit.json"))
                val br = BufferedReader(InputStreamReader(inputStream))
                val sb = br.use(BufferedReader::readText)
                br.close()
                return sb
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }


    fun encode64(message: String): String {
        return StringBuffer(Base64.getEncoder().encodeToString(StringBuffer(message).reverse().toString().toByteArray())).reverse().toString()
    }


    fun printShit() {
        for (i in 0 until message.length) {
            print((message[i].toInt() xor 20).toChar())
        }
        println()
    }


    fun isValidUTF8(input: ByteArray): Boolean {

        val cs = Charset.forName("UTF-8").newDecoder()
        return try {
            cs.decode(ByteBuffer.wrap(input))
            true
        } catch (e: CharacterCodingException) {
            false
        }

    }


    fun sendtoClip(string: String) {
        var clipboard = Clipboard.getSystemClipboard();
        var content = ClipboardContent();
        content.putString(string)
        clipboard.setContent(content)
    }

    fun <T> getSelectedItem(tableView: TableView<T>): T? {
        val selected = tableView.selectionModel.selectedIndex
        return if (selected > -1) {
            tableView.selectionModel.selectedItem
        } else
            null
    }

    fun getSelectedIndex(tableView: TableView<*>): Int {
        return tableView.selectionModel.selectedIndex
    }


    fun isAcceptable(value: String): Boolean {
        return isBoolean(value) || isInt(value)
    }

    fun isBoolean(value: String): Boolean {
        return value.toLowerCase() == "true" || value.toLowerCase() == "false"
    }

    fun isInt(value: String): Boolean {
        return try {
            java.lang.Integer.parseInt(value)
            true
        } catch (e: Exception) {
            println(e)
            value.chars().allMatch({ Character.isDigit(it) })
        }
    }


    fun isValidIP(ipAddr: String): Boolean {
        val ptn = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$")
        val mtch = ptn.matcher(ipAddr)
        return mtch.find()
    }

    fun isDecimal(value: Any?): Boolean {
        return try {
            java.lang.Double.parseDouble(value.toString())
            true
        }catch (e: Exception) {
            false
        }
    }

    internal object ValidateIPV4 {

        private val IPV4_REGEX = "(([0-1]?[0-9]{1,2}\\.)|(2[0-4][0-9]\\.)|(25[0-5]\\.)){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))"
        private val IPV4_PATTERN = Pattern.compile(IPV4_REGEX)

        fun isValidIPV4(s: String): Boolean {
            return IPV4_PATTERN.matcher(s).matches()
        }
    }

}