package com.supreme.helpers

import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.IOUtils

import java.io.File
import java.io.FileInputStream
import java.io.IOException

class GetMD5ForFile {

    private var file: File? = null

    // md5Hex converts an array of bytes into an array of characters representing the hexadecimal values of each byte in order.
    // The returned array will be double the length of the passed array, as it takes two characters to represent any given byte.
    val mD5: String?
        get() {
            var md5: String? = null

            var fileInputStream: FileInputStream? = null

            try {
                fileInputStream = FileInputStream(this.file!!)

                md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fileInputStream))

                fileInputStream.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }

            return md5
        }

    constructor(filePath: String) {
        this.file = File(filePath)
    }

    constructor(file: File) {
        this.file = file
    }

}
