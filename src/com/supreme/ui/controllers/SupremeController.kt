package com.supreme.ui.controllers


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.supreme.client.DebugClient
import com.supreme.client.OverrideAuth
import com.supreme.client.OverridePacket
import com.supreme.helpers.GetMD5ForFile
import com.supreme.helpers.SupremeHelper
import com.supreme.helpers.SupremeHelper.getSelectedIndex
import com.supreme.helpers.SupremeHelper.getSelectedItem
import com.supreme.helpers.SupremeHelper.sendtoClip
import com.supreme.models.*
import com.supreme.models.table.*
import com.supreme.network.*
import com.supreme.ui.control.MessageBox
import io.reactivex.schedulers.Schedulers
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.scene.input.DragEvent
import javafx.scene.input.MouseEvent
import javafx.scene.input.TransferMode
import javafx.scene.layout.Pane
import javafx.util.Pair
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.awt.Desktop
import java.io.File
import java.io.FileOutputStream
import java.net.URISyntaxException
import java.net.URL
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.HashMap


class SupremeController : DebugClient.OnDebugListener {
    override fun onDebugResult(debug: Debug) {
        getDebugRows().add(debug)
    }

    override fun onDebugClose() {

        MessageBox.Builder(Alert.AlertType.INFORMATION, "Debugger Closed", "You have disconnected from the debugger").show()
    }

    override fun onDebugConnected() {
        MessageBox.Builder(Alert.AlertType.INFORMATION, "Debugger Connected", "You have connected to the debugger on supreme.").show()
    }


    @FXML
    lateinit var authTab: Tab
    @FXML
    lateinit var permissionTab: Tab
    @FXML
    lateinit var settingTab: Tab
    @FXML
    lateinit var characterTab: Tab
    @FXML
    lateinit var userTab: Tab
    @FXML
    lateinit var buddyTab: Tab
    @FXML
    lateinit var versionTab: Tab
    @FXML
    lateinit var debugTab: Tab
    @FXML
    lateinit var searchBox: TextField
    @FXML
    lateinit var fbCheckBox: CheckBox
    @FXML
    lateinit var searchBtn: Button
    @FXML
    lateinit var nextBtn: Button
    @FXML
    lateinit var prevBtn: Button
    @FXML
    lateinit var mCharacterTableView: TableView<CharacterRow>
    @FXML
    lateinit var mPermissionTableView: TableView<PermissionRow>
    @FXML
    lateinit var mBuddyTableView: TableView<BuddyRow>
    @FXML
    lateinit var mSettingsTableView: TableView<Pair<String, String>>
    @FXML
    private lateinit var mAuthorizedTableView: TableView<AuthRow>
    @FXML
    private lateinit var mVersionTableView: TableView<VersionRow>
    @FXML
    private lateinit var mUserTableView: TableView<UserRow>
    @FXML
    private lateinit var mDebugTableView: TableView<Debug>
    @FXML
    lateinit var settingPidLabel: Label
    @FXML
    lateinit var settingValueField: TextField
    @FXML
    lateinit var settingSetBtn: Button
    @FXML
    lateinit var settingFinalizeBtn: Button
    @FXML
    lateinit var settingRestoreBtn: Button
    @FXML
    lateinit var tabPane: TabPane

    @FXML
    lateinit var vDropPane: Pane

    private var mDebugClient: DebugClient = DebugClient(2002)

    private lateinit var authService: YoAuthService
    private lateinit var characterService: YoCharacterService
    private lateinit var buddyService: YoBuddyService
    private lateinit var versionService: VersionService
    private lateinit var userService: UserService
    private lateinit var permissionService: PermissionService

    private val authRows = FXCollections.observableArrayList(ArrayList<AuthRow>())
    private val characterRows = FXCollections.observableArrayList(ArrayList<CharacterRow>())
    private val buddyRows = FXCollections.observableArrayList(ArrayList<BuddyRow>())
    private val settingRows = FXCollections.observableArrayList(ArrayList<Pair<String, String>>())
    private val versionRows = FXCollections.observableArrayList(ArrayList<VersionRow>())
    private val userRows = FXCollections.observableArrayList(ArrayList<UserRow>())
    private val permissionRows = FXCollections.observableArrayList(ArrayList<PermissionRow>())
    private val debugRows = FXCollections.observableArrayList(ArrayList<Debug>())


    private val mAuthorizedMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())
    private val mCharacterMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())
    private val mBuddyMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())
    private val mVersionMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())
    private val mUserMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())
    private val mPermissonMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())
    private val mDebugMenuItemList = FXCollections.observableArrayList(ArrayList<MenuItem>())


    private val mAuthorizedContextMenu = ContextMenu()
    private val mCharacterContextMenu = ContextMenu()
    private val mBuddyContextMenu = ContextMenu()
    private val mVersionContextMenu = ContextMenu()
    private val mUserContextMenu = ContextMenu()
    private val mPermissionContextMenu = ContextMenu()
    private val mDebugContextMenu = ContextMenu()


    private var selectedPlayerId = 0
    private var selectedUserId = 0


    val BASE_URL = "http://s1.supreme-one.net:2052/"
    lateinit var URL: String


    internal var userPage = 1
    internal var authPage = 1
    internal var characterPage = 1
    internal var buddyPage = 1
    internal var versionPage = 1


    var file: File? = null

    @FXML
    lateinit var vCodeNameTextBox: TextField
    @FXML
    lateinit var vVersionTextBox: TextField
    @FXML
    lateinit var vMD5TextBox: TextField
    @FXML
    lateinit var vSizeTextBox: TextField
    @FXML
    lateinit var vSubmitBtn: Button

    private val DEFAULT = object : HashMap<String, Any?>() {
        init {
            put("rainbow", false)
            put("canttt", false)
            put("laglessroom", false)
            put("laglessclothes", false)
            put("supremeentrance", false)
            put("copycat", false)
            put("ignore", ArrayList<Any>())
            put("spoofer", false)
            put("stalker", false)
            put("canfreeze", false)
            put("chaosmode", false)
            put("frequency", 0)
            put("lfrequency", -1)
            put("summoned", null)
            put("chatrepeater", false)
        }
    }

    private var CURRENT: HashMap<String, Any?>? = HashMap()

    //region Queries

    fun getUsers(params: HashMap<String, Any> = HashMap()) {

        userService.search(params)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ res ->
                    userRows.setAll(res.data?.map { UserRow(it) }?.toList())
                }, System.err::println)
    }

    fun getBuddies(id: Int, params: HashMap<String, Any> = HashMap()) {

        buddyService.search(id, params)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ res ->
                    buddyRows.setAll(res.data?.map { BuddyRow(it) }?.toList())
                }, System.err::println)
    }

    fun getVersions(params: HashMap<String, Any> = HashMap()) {
        versionService.search(params)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ res ->
                    versionRows.setAll(res.data?.map { VersionRow(it) }?.toList())
                }, System.err::println)
    }

    fun getSupremes(params: HashMap<String, Any> = HashMap()) {
        authService.search(params)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ res -> authRows.setAll(res.data?.map { AuthRow(it) }?.toList()) }, System.err::println)
    }

    fun getCharacters(params: HashMap<String, Any> = HashMap()) {
        characterService.search(params)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ res -> characterRows.setAll(res.data?.map { CharacterRow(it) }?.toList()) }, System.err::println)
    }

    fun getPermission(user_id: Int, params: HashMap<String, Any> = HashMap()) {
        permissionService.search(user_id, params)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ res -> permissionRows.setAll(res.data?.map { PermissionRow(it) }?.toList()) }, System.err::println)
    }
    //endregion

    //region Context Menu

    fun getPermissionMenu(): ContextMenu {
        if (mPermissionContextMenu.items.size == 0) {
            val sync = MenuItem("Sync")
            val api = MenuItem("API Detail")
            val approve = MenuItem("Approve")
            val deny = MenuItem("Deny")

            val iPermissionHandler = EventHandler<ActionEvent> {
                val row = getSelectedItem(mPermissionTableView)
                val selectedIndex = getSelectedIndex(mPermissionTableView)
                if (row != null) {
                    try {
                        when (it.source) {
                            sync -> {
                                permissionService.get(row.user_id, row.mod_hash).subscribe({ res -> mPermissionTableView.items[selectedIndex] = PermissionRow(res.data) }) { error -> }
                            }
                            api -> {
                                Desktop.getDesktop().browse(URL(String.format("%sapi/v1/user/%s/permission/%s", URL, row.user_id, row.mod_hash)).toURI())
                            }
                            approve -> {
                                permissionService.update(row.user_id, row.mod_hash, object : HashMap<String, Any>() {
                                    init {
                                        put("permission", true)
                                        put("message", "You were approved for the mod.")
                                        put("status", "APPROVED")
                                    }
                                }).subscribe({ res -> mPermissionTableView.items[selectedIndex] = PermissionRow(res.data) }) { error -> }
                            }
                            deny -> {
                                permissionService.update(row.user_id, row.mod_hash, object : HashMap<String, Any>() {
                                    init {
                                        put("permission", false)
                                        put("message", "You were denied for the mod.")
                                        put("status", "DENIED")
                                    }
                                }).subscribe({ res -> mPermissionTableView.items[selectedIndex] = PermissionRow(res.data) }) { error -> }
                            }
                        }

                    } catch (e: URISyntaxException) {
                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                    } catch (e: Exception) {
                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                    }
                } else {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "We could not restore the data because no field was selected in the Authorized Tab.").show()
                }
            }

            sync.onAction = iPermissionHandler
            api.onAction = iPermissionHandler
            approve.onAction = iPermissionHandler
            deny.onAction = iPermissionHandler
            mPermissonMenuItemList.setAll(sync, api, approve, deny)
            mPermissionContextMenu.items.setAll(mPermissonMenuItemList)
        }
        return mPermissionContextMenu
    }

    fun getUserMenu(): ContextMenu {
        if (mUserContextMenu.items.size == 0) {
            val sync = MenuItem("Sync")
            val api = MenuItem("API Detail")
            val ban = MenuItem("Ban")
            val unban = MenuItem("Unban")
            val user = MenuItem("Make User")
            val mod = MenuItem("Make Moderator")
            val admin = MenuItem("Make Administrator")
            val yoaccounts = MenuItem("Fetch YoAccounts")
            val permissions = MenuItem("Fetch Permissions")
            val give_permissions = MenuItem("Give Permission")
            val settings = MenuItem("Edit Settings")
            val say = MenuItem("Say Something")
            val adminMsg = MenuItem("Send Admin Msg")
            val globalAdminMsg = MenuItem("Send Global Admin Msg")
            val setip = MenuItem("Set w/ IP")

            val iUserHandler = EventHandler<ActionEvent> {
                val row = getSelectedItem(mUserTableView)
                val selectedIndex = getSelectedIndex(mUserTableView)
                if (row != null) {
                    try {
                        when (it.source) {
                            sync -> {
                                userService.get(row.user.id).subscribe({ res -> mUserTableView.items[selectedIndex] = UserRow(res.data) }) { error -> }
                            }
                            api -> {
                                Desktop.getDesktop().browse(URL(String.format("%sapi/v1/user/%s", URL, row.user.id)).toURI())
                            }
                            ban -> {
                                userService.update(row.user.id, object : HashMap<String, Any>() {
                                    init {
                                        put("user_banned", 1)
                                    }
                                }).subscribe({ res -> mUserTableView.items[selectedIndex] = UserRow(res.data) }) { error -> }
                            }
                            unban -> {
                                userService.update(row.user.id, object : HashMap<String, Any>() {
                                    init {
                                        put("user_banned", 0)
                                    }
                                }).subscribe({ res -> mUserTableView.items[selectedIndex] = UserRow(res.data) }) { error -> }
                            }
                            say -> {
                                val mUserRow = mUserTableView.items[selectedIndex]
                                if (mUserRow.user.active) {
                                    val result = MessageBox.Input("Supreme Override", "What do you want this user to say?", "You can make them say anything").showAndWait()
                                    if (!result.isPresent) {
                                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Message prompt should not be null or empty").showAndWait()
                                        return@EventHandler
                                    }
                                    val message = result.get()
                                    val user_id = mUserRow.user.id;
                                    val s1_token = mUserRow.user.api_token;
                                    userService.getActiveAccounts(user_id).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                            .subscribe({ response ->
                                                response.data?.forEach { auth ->
                                                    val override_auth = OverrideAuth(s1_token, auth.api_token)
                                                    mDebugClient.sendOverrideMessage(OverridePacket.sendMessage(override_auth, message));
                                                }
                                            }, {}, {})

                                } else {
                                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "User is not active").showAndWait();
                                }
                            }
                            adminMsg -> {
                                val mUserRow = mUserTableView.items[selectedIndex]
                                if (mUserRow.user.active) {
                                    val messageResult = MessageBox.Input("Supreme Override", "What do you want to say to this user", "Send your admin message to them").showAndWait()
                                    val titleResult = MessageBox.Input("Supreme Override", "What is the title of this message?", "ie. Supreme Admin MSG (Hit Cancel for default)").showAndWait()
                                    val fromResult = MessageBox.Input("Supreme Override", "Who is this admin message from?", "ie. Supreme Blackman. (Hit Cancel for default)").showAndWait()
                                    if (!messageResult.isPresent) {
                                        return@EventHandler
                                    }
                                    val message = messageResult.get()
                                    var from = "- The Higher Ups"
                                    var title = "Supreme Admin Msg"

                                    if (titleResult.isPresent) {
                                        title = titleResult.get()
                                    }
                                    if (fromResult.isPresent) {
                                        from = fromResult.get()
                                    }
                                    val user_id = mUserRow.user.id;
                                    val s1_token = mUserRow.user.api_token;
                                    if (!message.isNullOrEmpty()) {
                                        userService.getActiveAccounts(user_id).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                                .subscribe({ response ->
                                                    response.data?.forEach { auth ->
                                                        val override_auth = OverrideAuth(s1_token, auth.api_token)
                                                        mDebugClient.sendOverrideMessage(OverridePacket.sendAdminMessage(override_auth, message!!, title, from));
                                                    }
                                                }, {}, {})
                                    } else {
                                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Message prompt should not be null or empty").showAndWait();
                                    }
                                } else {
                                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "User is not active").showAndWait();
                                }
                            }
                            globalAdminMsg -> {
                                val messageResult = MessageBox.Input("Supreme Override", "What do you want to say to all users", "Send your admin message to them").showAndWait()
                                if (!messageResult.isPresent) {
                                    return@EventHandler
                                }
                                val message = messageResult.get()
                                if (message.isNotEmpty()) {
                                    mDebugClient.sendSoftOverrideMessage(OverridePacket.sendGlobalAdminMessage(message));
                                } else {
                                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Message prompt should not be null or empty").showAndWait();
                                }
                            }
                            user -> {
                                userService.update(row.user.id, object : HashMap<String, Any>() {
                                    init {
                                        put("level", 0)
                                    }
                                }).subscribe({ res -> mUserTableView.items[selectedIndex] = UserRow(res.data) }) { error -> }
                            }
                            mod -> {
                                userService.update(row.user.id, object : HashMap<String, Any>() {
                                    init {
                                        put("level", 69)
                                    }
                                }).subscribe({ res -> mUserTableView.items[selectedIndex] = UserRow(res.data) }) { error -> }
                            }
                            admin -> {
                                userService.update(row.user.id, object : HashMap<String, Any>() {
                                    init {
                                        put("level", 80085)
                                    }
                                }).subscribe({ res -> mUserTableView.items[selectedIndex] = UserRow(res.data) }) { error -> }

                            }
                            settings -> {
                                var userSettings = row.user.userSettings
                                if (userSettings == null) {
                                    userSettings = DEFAULT
                                    MessageBox.Builder(Alert.AlertType.WARNING, "Warning", "User has not gone on swf. setting default settings").show()
                                }
                                settingPidLabel.text = row.user.name
                                selectedUserId = row.user.id
                                tabPane.selectionModel.select(settingTab)
                                settingTab.isDisable = false
                                val obj = userSettings.entries.stream()
                                        .map { res ->
                                            if (SupremeHelper.isDecimal(res.value)) {
                                                Pair<String, String>(res.key, if (res.value != null) res.value.toString().toDouble().toInt().toString() else null)
                                            } else {
                                                Pair<String, String>(res.key, if (res.value != null) res.value.toString() else null)
                                            }
                                        }
                                        .filter { it.value != null && it.key != "defaultroom" && it.key != "id" }
                                        .collect(Collectors.toList<Pair<String, String>>())
                                settingRows.setAll(obj)

                            }

                            yoaccounts -> {
                                tabPane.selectionModel.select(authTab)
                                getSupremes(object : HashMap<String, Any>() {
                                    init {
                                        put("assigned_to", row.user.id)
                                    }
                                })
                            }
                            give_permissions -> {
                                val input = MessageBox.Input("Permission", "Paste", "MD5 Here").showAndWait()
                                input.ifPresent { hash ->
                                    run {
                                        permissionService.store(row.user.id, object : HashMap<String, Any>() {
                                            init {
                                                put("mod_hash", hash)
                                            }
                                        }).subscribeOn(Schedulers.io())
                                                .flatMap {
                                                    permissionService.update(row.user.id, hash, object : HashMap<String, Any>() {
                                                        init {
                                                            put("permission", true)
                                                            put("message", "You were approved for the mod.")
                                                            put("status", "APPROVED")
                                                        }
                                                    })
                                                }
                                                .observeOn(Schedulers.trampoline())
                                                .subscribe({
                                                    tabPane.selectionModel.select(permissionTab)
                                                    permissionTab.isDisable = false
                                                    getPermission(row.user.id)
                                                }, {

                                                })
                                    }
                                }

                            }
                            permissions -> {
                                tabPane.selectionModel.select(permissionTab)
                                permissionTab.isDisable = false
                                getPermission(row.user.id)
                            }
                            setip -> {
                                val result = MessageBox.Input("Supreme User IP Change", "Set IP", "Authorized User IP.", "<currentip>").showAndWait()
                                result.ifPresent {
                                    when {
                                        it == "<currentip>" || SupremeHelper.ValidateIPV4.isValidIPV4(it) -> {
                                            userService.update(row.user.id, object : HashMap<String, Any>() {init {
                                                put("user_ip", it)
                                            }
                                            }).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                                    .subscribe({ mUserTableView.items[selectedIndex] = UserRow(it.data) }, {}, {})
                                        }
                                        else -> MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Not a valid IP Address.").showAndWait()
                                    }
                                }
                            }
                        }

                    } catch (e: URISyntaxException) {
                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                    }
                } else {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "We could not restore the data because no field was selected in the Authorized Tab.").show()
                }
            }

            settings.onAction = iUserHandler
            sync.onAction = iUserHandler
            api.onAction = iUserHandler
            ban.onAction = iUserHandler
            unban.onAction = iUserHandler
            user.onAction = iUserHandler
            mod.onAction = iUserHandler
            admin.onAction = iUserHandler
            yoaccounts.onAction = iUserHandler
            setip.onAction = iUserHandler
            permissions.onAction = iUserHandler
            give_permissions.onAction = iUserHandler
            say.onAction = iUserHandler
            adminMsg.onAction = iUserHandler
            globalAdminMsg.onAction = iUserHandler
            mUserMenuItemList.setAll(sync, api, yoaccounts, user, mod, admin, setip, settings, unban, ban, permissions, give_permissions, say, adminMsg, globalAdminMsg)
            mUserContextMenu.items.setAll(mUserMenuItemList)
        }
        return mUserContextMenu
    }

    fun getAuthorizedMenu(): ContextMenu {
        if (mAuthorizedContextMenu.items.size == 0) {
            val sync = MenuItem("Sync")
            val api = MenuItem("API Detail")
            val fb = MenuItem("View FB")
            val ban = MenuItem("Ban")
            val unban = MenuItem("Unban")
            val reset = MenuItem("Reset")
            val user = MenuItem("Make User")
            val mod = MenuItem("Make Moderator")
            val admin = MenuItem("Make Administrator")
            val remove = MenuItem("Undo Supreme")
            val buddies = MenuItem("Fetch Buddies")
            val setip = MenuItem("Set w/ IP")
            val assignto = MenuItem("Assign to...")

            val iAuthHandler = EventHandler<ActionEvent> {
                val row = getSelectedItem(mAuthorizedTableView)
                val selectedIndex = getSelectedIndex(mAuthorizedTableView)
                if (row != null) {
                    try {
                        when (it.source) {
                            assignto -> {

                                val result = MessageBox.Input("Assign Yo Account to User Id", "Set User Id", "Assign To User ID:").showAndWait()
                                result.ifPresent {

                                    authService.update(row.auth.userId, object : HashMap<String, Any>() {init {
                                        put("assigned_to", it)
                                    }
                                    })
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(Schedulers.trampoline())
                                            .subscribe({ mAuthorizedTableView.items[selectedIndex] = AuthRow(it.data) }, {}, {})

                                }
                            }
                            sync -> {
                                authService.get(row.auth.userId).subscribe({ res -> mAuthorizedTableView.items[selectedIndex] = AuthRow(res.data) }) { error -> }
                            }
                            api -> {
                                var url = String.format("%sapi/v1/supreme/%s", URL, row.auth.userId)
                                sendtoClip(url);
                                Desktop.getDesktop().browse(URL(url).toURI())

                            }
                            fb -> {
                                Desktop.getDesktop().browse(URL(String.format("https://facebook.com/%s", row.auth.userFacebookId)).toURI())

                            }
                            ban -> {
                                authService.update(row.auth.userId, object : HashMap<String, Any>() {
                                    init {
                                        put("user_banned", 1)
                                    }
                                }).subscribe({ res -> mAuthorizedTableView.items[selectedIndex] = AuthRow(res.data) }) { error -> }
                            }
                            unban -> {
                                authService.update(row.auth.userId, object : HashMap<String, Any>() {
                                    init {
                                        put("user_banned", 0)
                                    }
                                }).subscribe({ res -> mAuthorizedTableView.items[selectedIndex] = AuthRow(res.data) }) { error -> }
                            }
                            reset -> {
                                authService.settings(row.auth.userId, object : HashMap<String, Any?>() {
                                    init {
                                        put("user_settings", object : HashSet<Any?>() {
                                            init {
                                                put("rainbow", false)
                                                put("canttt", false)
                                                put("laglessroom", false)
                                                put("laglessclothes", false)
                                                put("supremeentrance", false)
                                                put("copycat", false)
                                                put("ignore", ArrayList<Any>())
                                                put("spoofer", false)
                                                put("stalker", false)
                                                put("canfreeze", false)
                                                put("chaosmode", false)
                                                put("frequency", 0)
                                                put("lfrequency", -1)
                                                put("summoned", null)
                                                put("chatrepeater", false)
                                            }
                                        })
                                    }
                                }).flatMap<YoResponse<YoAuth, PaginationData>> { yoAuthPaginationDataYoResponse -> authService.get(row.auth.userId) }.subscribe({ resource -> mAuthorizedTableView.items[selectedIndex] = AuthRow(resource.data) }) { error -> }

                            }
                            user -> {
                                authService.update(row.auth.userId, object : HashMap<String, Any>() {
                                    init {
                                        put("user_level", 0)
                                    }
                                }).subscribe({ res -> mAuthorizedTableView.items[selectedIndex] = AuthRow(res.data) }) { error -> }
                            }
                            mod -> {
                                authService.update(row.auth.userId, object : HashMap<String, Any>() {
                                    init {
                                        put("user_level", 69)
                                    }
                                }).subscribe({ res -> mAuthorizedTableView.items[selectedIndex] = AuthRow(res.data) }) { error -> }
                            }
                            admin -> {
                                authService.update(row.auth.userId, object : HashMap<String, Any>() {
                                    init {
                                        put("user_level", 80085)
                                    }
                                }).subscribe({ res -> mAuthorizedTableView.items[selectedIndex] = AuthRow(res.data) }) { error -> }

                            }
                            remove -> {

                                authService.delete(row.auth.userId)
                                        .subscribe({ res -> getSupremes(HashMap()) }) { error -> }
                            }
                            buddies -> {
                                tabPane.selectionModel.select(buddyTab)
                                buddyTab.isDisable = false
                                getBuddies(row.auth.userId, HashMap())
                            }
                            setip -> {
                                val result = MessageBox.Input("Supreme Authorization Part 2", "Set IP", "Authorized User IP.", "<currentip>").showAndWait()
                                result.ifPresent {
                                    when {
                                        it == "<currentip>" || SupremeHelper.ValidateIPV4.isValidIPV4(it) -> {
                                            authService.update(row.auth.userId, object : HashMap<String, Any>() {init {
                                                put("user_ip", it)
                                            }
                                            })
                                                    .subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                                    .subscribe({ mAuthorizedTableView.items[selectedIndex] = AuthRow(it.data) }, {}, {})
                                        }
                                        else -> MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Not a valid IP Address.").showAndWait()
                                    }
                                }
                            }
                        }

                    } catch (e: URISyntaxException) {
                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                    } catch (e: Exception) {
                        MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                    }
                } else {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "We could not restore the data because no field was selected in the Authorized Tab.").show()
                }
            }

            sync.onAction = iAuthHandler
            api.onAction = iAuthHandler
            fb.onAction = iAuthHandler
            ban.onAction = iAuthHandler
            unban.onAction = iAuthHandler
            reset.onAction = iAuthHandler
            user.onAction = iAuthHandler
            mod.onAction = iAuthHandler
            admin.onAction = iAuthHandler
            remove.onAction = iAuthHandler
            buddies.onAction = iAuthHandler
            setip.onAction = iAuthHandler
            assignto.onAction = iAuthHandler
            mAuthorizedMenuItemList.setAll(sync, api, fb, buddies, user, mod, admin, setip, assignto, reset, unban, ban, remove)
            mAuthorizedContextMenu.items.setAll(mAuthorizedMenuItemList)
        }
        return mAuthorizedContextMenu
    }

    fun getCharacterMenu(): ContextMenu {
        if (mCharacterContextMenu.items.size == 0) {
            val sync = MenuItem("Sync")
            val api = MenuItem("API Detail")
            val fb = MenuItem("View FB")
            val goSupreme = MenuItem("Go Supreme")
            val undoSupreme = MenuItem("Undo Supreme")
            val iCharacterHandler = EventHandler<ActionEvent> {
                try {
                    val row = getSelectedItem(mCharacterTableView)
                    when (it.source) {
                        api -> {
                            val url = String.format("%sapi/v1/characters/%s", URL, row!!.character.userId);
                            sendtoClip(url)
                            Desktop.getDesktop().browse(URL(url).toURI())
                        }
                        sync -> characterService.get(row!!.character.userId).subscribe({ res -> mCharacterTableView.items[getSelectedIndex(mCharacterTableView)] = CharacterRow(res.data) }) { error -> }
                        fb -> Desktop.getDesktop().browse(URL(String.format("https://facebook.com/%s", row!!.character.userFacebookId)).toURI())
                        goSupreme -> authService.create(
                                SupremeHelper.encode64(
                                        Gson().toJson(
                                                YoAuth.Builder.create()
                                                        .setUser_id(row!!.character.userId)
                                                        .setFb_id(row.character.userFacebookId)
                                                        .setUser_name(row.character.userName)
                                        )
                                ))
                                .flatMap<YoResponse<YoCharacter, PaginationData>> { characterService.get(row.character.userId) }
                                .subscribe({ res -> mCharacterTableView.items[getSelectedIndex(mCharacterTableView)] = CharacterRow(res.data) }, { error ->
                                    if (error is HttpException) {
                                        if (error.code() == 401) {
                                            getSupremes(HashMap())
                                        }
                                    }

                                }) { getSupremes(HashMap()) }
                        undoSupreme -> authService.delete(row!!.character.userId).flatMap<YoResponse<YoCharacter, PaginationData>> { res -> characterService.get(res.data!!.userId) }.subscribe({ res -> mCharacterTableView.items[getSelectedIndex(mCharacterTableView)] = CharacterRow(res.data) }) { }

                    }
                } catch (e: URISyntaxException) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                } catch (e: Exception) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                }
            }
            sync.onAction = iCharacterHandler
            api.onAction = iCharacterHandler
            fb.onAction = iCharacterHandler
            goSupreme.onAction = iCharacterHandler
            undoSupreme.onAction = iCharacterHandler
            mCharacterMenuItemList.setAll(sync, api, fb, goSupreme, undoSupreme)
            mCharacterContextMenu.items.setAll(mCharacterMenuItemList)
        }
        return mCharacterContextMenu
    }

    fun getBuddyMenu(): ContextMenu {
        if (mBuddyContextMenu.items.size == 0) {
            val sync = MenuItem("Sync")
            val api = MenuItem("API Detail")
            val fb = MenuItem("View FB")
            val goSupreme = MenuItem("Go Supreme")
            val undoSupreme = MenuItem("Undo Supreme")
            val iBuddyHandler = EventHandler<ActionEvent> {
                val row = getSelectedItem(mBuddyTableView)
                try {
                    when (it.source) {
                        sync -> {
                            buddyService.get(row!!.buddy.supreme_user_id, row.buddy.user_id).subscribe({ res -> mBuddyTableView.items[getSelectedIndex(mBuddyTableView)] = BuddyRow(res.data) }) { }
                        }
                        api -> {
                            val url = String.format("%sapi/v1/supreme/%s/buddy/%s", URL, getSelectedItem(mAuthorizedTableView)?.auth?.userId, row!!.buddy.user_id)
                            sendtoClip(url)
                            Desktop.getDesktop().browse(URL(url).toURI())
                        }
                        fb -> {
                            Desktop.getDesktop().browse(URL(String.format("https://facebook.com/%s", row!!.buddy.user_facebook_id)).toURI())
                        }
                        goSupreme -> {
                            authService.create(
                                    SupremeHelper.encode64(
                                            Gson().toJson(
                                                    YoAuth.Builder.create()
                                                            .setUser_id(row!!.buddy.user_id)
                                                            .setFb_id(row.buddy_fb)
                                                            .setUser_name(row.buddy_name)
                                            )
                                    ))
                                    .flatMap<YoResponse<YoBuddy, PaginationData>> { buddyService.get(row.buddy.supreme_user_id, row.buddy.user_id) }
                                    .subscribe({ res -> mBuddyTableView.items[getSelectedIndex(mBuddyTableView)] = BuddyRow(res.data) }, { error ->
                                        if (error is HttpException) {
                                            if (error.code() == 401) {
                                                getSupremes(HashMap())
                                            }
                                        }

                                    }) { getSupremes(HashMap()) }
                        }
                        undoSupreme -> {
                            authService.delete(row!!.buddy.user_id)
                                    .flatMap<YoResponse<YoBuddy, PaginationData>> { res -> buddyService.get(row.buddy.supreme_user_id, res.data!!.userId) }
                                    .subscribe({ res -> mBuddyTableView.items[getSelectedIndex(mBuddyTableView)] = BuddyRow(res.data) }) { error -> }

                        }


                    }
                } catch (e: URISyntaxException) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                } catch (e: Exception) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                }

            }
            sync.onAction = iBuddyHandler
            api.onAction = iBuddyHandler
            fb.onAction = iBuddyHandler
            goSupreme.onAction = iBuddyHandler
            undoSupreme.onAction = iBuddyHandler
            mBuddyMenuItemList.setAll(sync, api, fb, goSupreme, undoSupreme)
            mBuddyContextMenu.items.setAll(mBuddyMenuItemList)
        }
        return mBuddyContextMenu
    }

    fun getVersionMenu(): ContextMenu {
        if (mVersionContextMenu.items.size == 0) {
            val md5 = MenuItem("Copy MD5")
            val swfLink = MenuItem("Copy Swf Link")
            val setTechDigHash = MenuItem("Set Tech Dig Hash");
            val delete = MenuItem("Delete Version")
            val publish = MenuItem("Publish")
            val unpublish = MenuItem("Unpublish")
            val download = MenuItem("Download")
            val iVersionHandler = EventHandler<ActionEvent> {
                val row = getSelectedItem(mVersionTableView)
                try {
                    when (it.source) {
                        md5 -> {
                            sendtoClip(row!!.versionModel.md5)
                        }
                        swfLink -> {
                            sendtoClip(String.format("%sapi/v1/version/%s", URL, row!!.versionModel.md5))
                        }
                        setTechDigHash -> {
                            val result = MessageBox.Input("Assign Tech Dig", "Set Tech Dig", "Please set a valid tech dig", "").showAndWait()
                            result.ifPresent {
                                versionService.setTechDigHash(it, row!!.versionModel.md5).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                        .subscribe({
                                            if (it.isSuccessful) {
                                                val j = JSONObject(it.body()?.string());
                                                MessageBox.Builder(Alert.AlertType.INFORMATION, "Yotech Dig Set", j.getString("message")).show();
                                                getVersions();
                                            }
                                        }, {
                                            MessageBox.Builder(Alert.AlertType.INFORMATION, "Yotech Dig Error", it.localizedMessage).show();
                                        }, {})
                            }
                        }
                        delete -> {
                            versionService.delete(row!!.versionModel.md5).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                    .subscribe({
                                        getVersions()
                                    }, {
                                        Platform.runLater {
                                            MessageBox.Builder(Alert.AlertType.ERROR, "Supreme Error", it.message!!).show()
                                        }
                                    })
                        }
                        publish -> {
                            versionService.update(row!!.versionModel.md5, object : HashMap<String, Any>() {
                                init {
                                    put("publish", true)
                                }
                            }).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                    .subscribe({
                                        getVersions()
                                    }, {
                                        Platform.runLater {

                                            MessageBox.Builder(Alert.AlertType.ERROR, "Supreme Error", it.message!!).show()
                                        }
                                    })
                        }
                        unpublish -> {
                            versionService.update(row!!.versionModel.md5, object : HashMap<String, Any>() {
                                init {
                                    put("publish", false)
                                }
                            }).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline())
                                    .subscribe({
                                        getVersions()
                                    }, {
                                        Platform.runLater {
                                            MessageBox.Builder(Alert.AlertType.ERROR, "Supreme Error", it.message!!).show()
                                        }
                                    })
                        }
                        download -> {
                            versionService.get(row!!.versionModel.md5)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.trampoline())
                                    .subscribe({ res ->
                                        run {
                                            val codeName = res.headers()["X-Version-Name"] + ".swf"
                                            val file = File(codeName)
                                            val fileOutputStream = FileOutputStream(file)
                                            fileOutputStream.write(res.body()!!.bytes())
                                            fileOutputStream.flush()
                                            fileOutputStream.close()
                                            Platform.runLater {
                                                MessageBox.Builder(Alert.AlertType.INFORMATION, "Supreme Response", "Check the folder where this app is in.").show()
                                            }
                                        }
                                    }, { error ->
                                        Platform.runLater {

                                            MessageBox.Builder(Alert.AlertType.ERROR, "Supreme Error", "Could not download the swf").show()
                                        }
                                    })
                        }

                    }
                } catch (e: URISyntaxException) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                } catch (e: Exception) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                }

            }

            md5.onAction = iVersionHandler
            swfLink.onAction = iVersionHandler
            setTechDigHash.onAction = iVersionHandler
            delete.onAction = iVersionHandler
            publish.onAction = iVersionHandler
            unpublish.onAction = iVersionHandler
            download.onAction = iVersionHandler
            mVersionMenuItemList.setAll(md5, swfLink, setTechDigHash, publish, unpublish, download, delete)
            mVersionContextMenu.items.setAll(mVersionMenuItemList)
        }
        return mVersionContextMenu
    }


    fun getDebugMenu(): ContextMenu {
        if (mDebugContextMenu.items.size == 0) {
            val copy = MenuItem("Copy Data")
            val iDebugHandler = EventHandler<ActionEvent> {
                val row = getSelectedItem(mDebugTableView)
                try {
                    when (it.source) {
                        copy -> {
                            sendtoClip(row!!.data)
                        }
                    }
                } catch (e: URISyntaxException) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "URL format is incorrect").show()
                } catch (e: Exception) {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Unable to complete action").show()
                }

            }

            copy.onAction = iDebugHandler
            mDebugMenuItemList.add(copy)
            mDebugContextMenu.items.setAll(mDebugMenuItemList)
        }
        return mDebugContextMenu
    }

    //endregion

    //region Observables

    fun getPermissionRows(): ObservableList<PermissionRow> {
        return permissionRows
    }

    fun getDebugRows(): ObservableList<Debug> {
        return debugRows
    }

    fun getAuthRows(): ObservableList<AuthRow> {
        return authRows
    }

    fun getCharacterRows(): ObservableList<CharacterRow> {
        return characterRows
    }

    fun getBuddyRows(): ObservableList<BuddyRow> {
        return buddyRows
    }

    fun getSettingRows(): ObservableList<Pair<String, String>> {
        return settingRows
    }

    fun getVersionRows(): ObservableList<VersionRow> {
        return versionRows
    }

    fun getUserRows(): ObservableList<UserRow> {
        return userRows
    }
    //endregion


    fun onDebugStop(event: ActionEvent) {
        mDebugClient.stop()
    }


    fun onDebugClear(event: ActionEvent) {
        getDebugRows().clear()
    }


    fun onDebugConnect(event: ActionEvent) {
        mDebugClient.start()
    }


    fun submitSwf(event: ActionEvent) {

        val descriptionPart = RequestBody.create(MultipartBody.FORM, file!!.name)
        val fileP = RequestBody.create(MediaType.parse("application/x-shockwave-flash"), this.file!!)
        val filePart = MultipartBody.Part.createFormData("file", this.file!!.name, fileP)

        val md5 = RequestBody.create(
                MediaType.parse("text/plain"),
                vMD5TextBox.text)
        val codename = RequestBody.create(
                MediaType.parse("text/plain"),
                vCodeNameTextBox.text)
        val version = RequestBody.create(
                MediaType.parse("text/plain"),
                vVersionTextBox.text)

        val size = RequestBody.create(
                MediaType.parse("text/plain"),
                file!!.length().toString())
        val created_at = RequestBody.create(
                MediaType.parse("text/plain"),
                file!!.lastModified().toString())




        versionService.store(descriptionPart, filePart, md5, codename, version, size, created_at).subscribeOn(Schedulers.io()).observeOn(Schedulers.trampoline()).subscribe({ res ->
            run {
                getVersions(HashMap())

                Platform.runLater {

                    MessageBox.Builder(Alert.AlertType.INFORMATION, "Supreme Response", "Uploaded: " + res.data!!.codename).show()
                }
            }
        }, { error ->
            run {
                Platform.runLater {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Supreme Error", "Could not upload the swf: " + error.message).show()
                }
            }
        })

    }

    fun setSetting(event: ActionEvent) {
        val text = settingValueField.text
        val selectedItem = getSelectedItem(mSettingsTableView)
        val selectedIndex = getSelectedIndex(mSettingsTableView)
        if (selectedIndex > -1) {
            if (SupremeHelper.isAcceptable(text)) {
                if (SupremeHelper.isBoolean(selectedItem!!.value) && SupremeHelper.isBoolean(text) || SupremeHelper.isInt(selectedItem.value) && SupremeHelper.isInt(text)) {
                    val pair = Pair(selectedItem.key, text)
                    mSettingsTableView.items[selectedIndex] = pair
                } else {
                    MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Could not set data. Attempt to change data-type not acceptable.").show()
                }
            } else {
                MessageBox.Builder(Alert.AlertType.ERROR, "Error", "The data represented was not acceptable.").show()
            }
        } else {
            MessageBox.Builder(Alert.AlertType.ERROR, "Error", "Please select a row inorder to set its value.").show()
        }
    }

    fun restoreSettings(event: ActionEvent) {
        val row = getSelectedItem(mUserTableView)
        getSelectedIndex(mUserTableView)
        val userSettings = row!!.user.userSettings
        settingPidLabel.text = row.user.name
        selectedUserId = row.user.id
        tabPane.selectionModel.select(settingTab)
        settingTab.isDisable = false
        val obj = userSettings!!.entries.stream()
                .map { res -> Pair<String, String>(res.key, res.value.toString()) }
                .collect(Collectors.toList<Pair<String, String>>())
        settingRows.setAll(obj)
    }

    fun finalizeSettings(event: ActionEvent) {
        var items = mSettingsTableView.items
        for (pair in items) {
            when {
                SupremeHelper.isBoolean(pair.value) -> CURRENT!![pair.key] = java.lang.Boolean.parseBoolean(pair.value)
                SupremeHelper.isInt(pair.value) -> CURRENT!![pair.key] = Integer.parseInt(pair.value)
            }
        }
        authService.settings(selectedUserId, CURRENT)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.trampoline())
                .subscribe({ getUsers(HashMap()) }, System.err::println, {
                    selectedPlayerId = 0
                    CURRENT!!.clear()
                    settingTab.isDisable = true
                    tabPane.selectionModel.select(userPage)
                })
    }


    init {
        reinit(BASE_URL)
    }


    private fun reinit(url: String) {
        URL = url
        val httpLoggingInterceptor = HttpLoggingInterceptor { System.err.println(it) }
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    run {
                        val request = chain.request()
                        val newRequest = request.newBuilder().addHeader("Bypass", "1").build()
                        return@addInterceptor chain.proceed(newRequest)
                    }
                }
                .addInterceptor(httpLoggingInterceptor)
                .followSslRedirects(true)
                .followRedirects(true)
                .build()
        val retrofit = Retrofit.Builder().client(okHttpClient).baseUrl(URL).addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().setPrettyPrinting().create())).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
        authService = retrofit.create(YoAuthService::class.java)
        characterService = retrofit.create(YoCharacterService::class.java)
        buddyService = retrofit.create(YoBuddyService::class.java)
        versionService = retrofit.create(VersionService::class.java)
        userService = retrofit.create(UserService::class.java)
        permissionService = retrofit.create(PermissionService::class.java)

    }


    @FXML
    fun initialize() {
        mDebugClient.debugListener = this
        searchBox.setOnKeyReleased {
            search(ActionEvent(searchBtn, null))
        }

        vDropPane.onDragOver = EventHandler<DragEvent> { event ->
            run {
                if (event.gestureSource != vDropPane
                        && event.dragboard.hasFiles()) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(*TransferMode.COPY_OR_MOVE)

                }
                event.consume();
            }
        }
        vDropPane.onDragDropped = EventHandler<DragEvent> { event ->
            run {
                System.out.println("Over")
                val drag = event.dragboard

                var success = false
                if (drag.hasFiles()) {
                    this.file = drag.files[0]
                    vMD5TextBox.text = GetMD5ForFile(drag.files[0]).mD5!!.toLowerCase()
                    System.out.println(drag.files[0].absolutePath)

                    success = true
                }

                /* let the source know whether the string was successfully
                 * transferred and used */
                event.isDropCompleted = success

                event.consume()
            }
        }

        vDropPane.onDragDetected = EventHandler<MouseEvent> { event ->
            run {
                System.out.println("Detected")

                event.consume()
            }
        }

        settingTab.setOnSelectionChanged { event ->
            if (!settingTab.isSelected) {
                selectedPlayerId = 0
                CURRENT!!.clear()
                settingTab.isDisable = true

            }
        }
        buddyTab.setOnSelectionChanged { event ->
            if (!buddyTab.isSelected) {
                buddyTab.isDisable = true
            }
        }

        permissionTab.setOnSelectionChanged { event ->
            if (!permissionTab.isSelected) {
                permissionTab.isDisable = true
            }
        }
        getSupremes()
        getCharacters()
        getVersions()
        getUsers()
    }


    fun search(event: ActionEvent) {
        val map = HashMap<String, Any>()
        if (fbCheckBox.isSelected) {
            map["fb"] = 1
        }
        when {
            userTab.isSelected -> {
                when {
                    event.source === nextBtn -> userPage++
                    event.source === prevBtn -> {
                        if (userPage > 1) userPage--
                        else userPage = 1
                    }
                    else -> userPage = 1
                }
                map["page"] = userPage
                map["name"] = searchBox.text
                getUsers(map)
            }
            authTab.isSelected -> {
                when {
                    event.source === nextBtn -> authPage++
                    event.source === prevBtn -> {
                        if (authPage > 1) authPage--
                        else authPage = 1
                    }
                    else -> authPage = 1
                }
                map["page"] = authPage
                map["user_name"] = searchBox.text
                getSupremes(map)
            }
            characterTab.isSelected -> {
                when {
                    event.source === nextBtn -> characterPage++
                    event.source === prevBtn -> {
                        if (characterPage > 1) characterPage--
                        else characterPage = 1
                    }
                    else -> characterPage = 1
                }
                map["page"] = characterPage
                map["user_name"] = searchBox.text
                getCharacters(map)
            }
            buddyTab.isSelected -> {
                when {
                    event.source === nextBtn -> buddyPage++
                    event.source === prevBtn -> {
                        if (buddyPage > 1) buddyPage--
                        else buddyPage = 1
                    }
                    else -> buddyPage = 1
                }
                map["page"] = buddyPage
                map["user_name"] = searchBox.text
                val index = getSelectedIndex(mAuthorizedTableView)
                if (index > -1) {
                    val auth = getSelectedItem(mAuthorizedTableView)!!.auth
                    getBuddies(auth.userId, map)
                }
            }
            versionTab.isSelected -> {
                when {
                    event.source === nextBtn -> versionPage++
                    event.source === prevBtn -> {
                        if (versionPage > 1) versionPage--
                        else versionPage = 1
                    }
                    else -> versionPage = 1
                }
                map["page"] = versionPage
                map["codename"] = searchBox.text
                getVersions(map)
            }
        }
    }

}