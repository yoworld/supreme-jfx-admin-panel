package com.supreme.ui.control


import javafx.scene.control.Alert
import javafx.scene.control.TextInputDialog

/**
 * Created by cj on 1/16/16.
 */
object MessageBox {

    fun Builder(alertType: Alert.AlertType, title: String, content: String): Alert {
        val alert = Alert(alertType)
        alert.title = title
        alert.contentText = content
        return alert
    }

    fun Input(title: String = "Supreme", content: String, header: String, default: String = ""): TextInputDialog{
        val input = TextInputDialog(default)
        input.title = title
        input.contentText = content
        input.headerText = header
        return input
    }
}