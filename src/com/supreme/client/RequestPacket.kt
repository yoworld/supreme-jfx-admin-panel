package com.supreme.client

class RequestPacket<D>(var type: String, var message: String, var data: D)
