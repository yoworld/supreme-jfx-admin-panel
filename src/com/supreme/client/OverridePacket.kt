package com.supreme.client

class OverridePacket<D>(var message: String, var todo: String, var oauth: OverrideAuth, var data: D) {


    companion object {
        fun sendMessage(oauth: OverrideAuth, message: String): OverridePacket<RequestPacket<SendMessage>> {
            val sendMessage = SendMessage("public", message, 0);
            val requestPacket = RequestPacket("message", "Chat Emit Message", sendMessage);
            return OverridePacket<RequestPacket<SendMessage>>("debug", "emit.swf", oauth, requestPacket);
        }

        fun sendAdminMessage(oauth: OverrideAuth, message: String, title: String = "Supreme MSG", from: String = "Admin"): OverridePacket<RequestPacket<AdminMessage>> {
            val sendMessage = AdminMessage(title, message, from);
            val requestPacket = RequestPacket("adminmsg", "Chat Emit Admin Message", sendMessage);
            return OverridePacket("debug", "emit.swf", oauth, requestPacket);
        }

        fun sendGlobalAdminMessage(message: String): OverridePacket<String> {
            val oauth = OverrideAuth("nothing", "nothing")
            return OverridePacket("debug", "emit.admin.msg", oauth, message);
        }
    }

}