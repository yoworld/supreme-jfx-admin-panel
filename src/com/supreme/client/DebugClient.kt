package com.supreme.client

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.supreme.models.Debug
import javafx.application.Platform
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketException

class DebugClient @JvmOverloads constructor(private val ip: String = "s1.supreme-one.net", private val port: Int = 2002) : Runnable {
    private var socket: Socket = Socket()
    private var thread: Thread? = null
    var debugListener: OnDebugListener? = null

    constructor(port: Int) : this("s1.supreme-one.net", port)


    fun start() {
        if (thread == null) {
            thread = Thread(this)
            thread!!.start()
        }
    }

    fun stop() {
        if (thread != null) {
            socket.close()
            thread!!.interrupt()
        }
    }

    interface OnDebugListener {
        fun onDebugResult(debug: Debug)
        fun onDebugClose()
        fun onDebugConnected()
    }


    override fun run() {
        try {
            socket = Socket(ip, port)
            socket.getOutputStream().write(java.lang.String("{\"type\": \"init\", \"message\": \"debug\"}\n").bytes)
            socket.getOutputStream().flush()
            println("Wrote")
            if (debugListener != null) {
                Platform.runLater { debugListener!!.onDebugConnected() }
            }
            while (!thread!!.isInterrupted) {
                try {

                    if (!socket.isClosed && socket.getInputStream().available() > 0) {
                        System.out.println("Has data")
                        var message = socket.getInputStream().bufferedReader().readLine()
                        System.out.println(message)
                        if (message.trim().isNotEmpty()) {
                            if (debugListener != null) {
                                try {
                                    var fromJson = GsonBuilder().setLenient().create().fromJson(message, Debug::class.java)
                                    Platform.runLater { debugListener!!.onDebugResult(fromJson) }
                                } catch (e: Exception) {
                                    try {
                                        var json = JSONObject(message)
                                        Platform.runLater { debugListener!!.onDebugResult(Debug(json.getString("type"), json.getString("message"), json.optString("data"))) }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            }
                            System.out.println("Debug Message: " + message)
                        }

                    } else if (socket.isClosed) {
                        System.out.println("Error: Closed");
                        stop()

                    }
                } catch (e: IOException) {
                    System.out.print("Error: " + e.message)
                } catch (e: SocketException) {
                    System.out.println("Error: " + e.message)
                }
            }
            System.out.print("FAILED")
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            System.out.print("CLEANING UP")
            thread = null
            if (debugListener != null) {
                Platform.runLater { debugListener!!.onDebugClose() }
            }
        }

    }

    fun <D> sendOverrideMessage(sendMessage: OverridePacket<RequestPacket<D>>) {
        Thread {
            if (socket.isConnected) {
                val data = GsonBuilder().disableHtmlEscaping().create().toJson(sendMessage, object : TypeToken<OverridePacket<RequestPacket<D>>>() {}.type) + "\n"
                socket.getOutputStream().write(data.toByteArray())
                socket.getOutputStream().flush()
            }
        }.start();
    }

    fun <D> sendSoftOverrideMessage(sendMessage: OverridePacket<D>) {
        Thread {
            if (socket.isConnected) {
                val data = GsonBuilder().disableHtmlEscaping().create().toJson(sendMessage, object : TypeToken<OverridePacket<D>>() {}.type) + "\n"
                socket.getOutputStream().write(data.toByteArray())
                socket.getOutputStream().flush()
            }
        }.start();
    }


}
