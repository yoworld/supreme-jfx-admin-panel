package com.supreme.models;

public class YoBuddy {
    int user_id;
    int supreme_user_id;
    String user_name;
    int user_is_online;
    String user_room_name;
    String user_last_login;
    int category_id;
    int instance_id;
    String user_facebook_name;
    long user_facebook_id;
    int user_online_status;
    String saved_category_id;
    String visible_room_name;
    String created_at;
    String updated_at;
    String user_facebook_page;

    public int getSupreme_user_id() {
        return supreme_user_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public int getUser_is_online() {
        return user_is_online;
    }

    public String getUser_room_name() {
        return user_room_name;
    }

    public String getUser_last_login() {
        return user_last_login;
    }

    public int getCategory_id() {
        return category_id;
    }

    public int getInstance_id() {
        return instance_id;
    }

    public String getUser_facebook_name() {
        return user_facebook_name;
    }

    public long getUser_facebook_id() {
        return user_facebook_id;
    }

    public int getUser_online_status() {
        return user_online_status;
    }

    public String getSaved_category_id() {
        return saved_category_id;
    }

    public String getVisible_room_name() {
        return visible_room_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getUser_facebook_page() {
        return user_facebook_page;
    }
}
