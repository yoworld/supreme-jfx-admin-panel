package com.supreme.models;

import java.util.Objects;

public class Debug {
    String type;
    String message;
    String data;

    public Debug(String type, String message, String data){
        this.type = type;
        this.message = message;
        this.data = data;
    }

    public String getType() {
        return type != null ? type : "";
    }

    public String getMessage() {
        return message != null ? message : "";
    }

    public String getData() {
        return data != null ? data : "";
    }
}
