package com.supreme.models;

public class PaginationData {
    int current_page;
    String first_page_url;
    int from;
    int last_page;
    String last_page_url;
    String next_page_url;
    String path;
    String per_page;
    String prev_page_url;
    int to;
    int total;

    public int getCurrent_page() {
        return current_page;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public int getFrom() {
        return from;
    }

    public int getLast_page() {
        return last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public String getPath() {
        return path;
    }

    public String getPer_page() {
        return per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public int getTo() {
        return to;
    }

    public int getTotal() {
        return total;
    }
}
