package com.supreme.models;

import java.util.HashMap;

public class User {

    int id;
    String name;
    String email;
    String user_ip;
    String user_ipv6;
    int user_banned;
    String api_token;
    String created_at;
    String updated_at;
    Boolean active;
    String mod_title;
    HashMap<String, Object> user_settings = new HashMap<>();
    int level;

    public HashMap<String, Object> getUserSettings() {
        return user_settings;
    }

    public Boolean getActive() {
        return active;
    }

    public String getMod_title() {
        return mod_title;
    }

    public int getId() {
        return id;
    }

    public User setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUser_ip() {
        return user_ip;
    }

    public User setUser_ip(String user_ip) {
        this.user_ip = user_ip;
        return this;
    }

    public String getUser_ipv6() {
        return user_ipv6;
    }

    public User setUser_ipv6(String user_ipv6) {
        this.user_ipv6 = user_ipv6;
        return this;
    }

    public boolean isUser_banned() {
        return user_banned == 1;
    }

    public User setUser_banned(int user_banned) {
        this.user_banned = user_banned;
        return this;
    }

    public String getApi_token() {
        return api_token;
    }

    public User setApi_token(String api_token) {
        this.api_token = api_token;
        return this;
    }

    public String getCreated_at() {
        return created_at;
    }

    public User setCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public User setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
        return this;
    }

    public int getLevel() {
        return level;
    }

    public User setLevel(int level) {
        this.level = level;
        return this;
    }
}
