package com.supreme.models.table;

import com.supreme.models.YoAuth;
import com.supreme.models.YoCharacter;
import javafx.beans.property.SimpleStringProperty;

public class CharacterRow {


    private SimpleStringProperty char_gender;
    private SimpleStringProperty char_uid;
    private SimpleStringProperty char_name;
    private SimpleStringProperty char_login;
    private SimpleStringProperty char_created;
    private SimpleStringProperty char_fb;
    private YoCharacter character;

    public String getChar_gender() {
        return char_gender.get();
    }

    public SimpleStringProperty char_genderProperty() {
        return char_gender;
    }

    public String getChar_uid() {
        return char_uid.get();
    }

    public SimpleStringProperty char_uidProperty() {
        return char_uid;
    }

    public String getChar_name() {
        return char_name.get();
    }

    public SimpleStringProperty char_nameProperty() {
        return char_name;
    }

    public String getChar_login() {
        return char_login.get();
    }

    public SimpleStringProperty char_loginProperty() {
        return char_login;
    }

    public String getChar_created() {
        return char_created.get();
    }

    public SimpleStringProperty char_createdProperty() {
        return char_created;
    }

    public String getChar_fb() {
        return char_fb.get();
    }

    public SimpleStringProperty char_fbProperty() {
        return char_fb;
    }

    public CharacterRow(YoCharacter character) {
        this.character = character;
        this.char_uid = new SimpleStringProperty("<char_uid>");
        this.char_name = new SimpleStringProperty("<char_name>");
        this.char_login = new SimpleStringProperty("<char_login>");
        this.char_created = new SimpleStringProperty("<char_created>");
        this.char_fb = new SimpleStringProperty("<char_fb>");
        this.char_gender = new SimpleStringProperty("<char_gender>");


        this.char_uid.set(String.valueOf(character.getUserId()));
        this.char_name.set(character.getUserName());
        this.char_gender.set(character.getUserGender() == 2 ? "Male" : "Female");
        this.char_login.set(String.valueOf(character.getUserLastLogin()));
        this.char_created.set(String.valueOf(character.getUserCreatedOn()));
        this.char_fb.set(String.valueOf(character.getUserFacebookId()));
    }

    public YoCharacter getCharacter() {
        return character;
    }

    public void setCharacter(YoCharacter character) {
        this.character = character;
    }
}
