package com.supreme.models.table;

import com.supreme.models.User;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class UserRow {

    private final SimpleStringProperty mod_title;
    private SimpleStringProperty id;
    private  SimpleStringProperty level;
    private  SimpleStringProperty name;
    private  SimpleStringProperty email;
    private  SimpleStringProperty user_ip;
    private  SimpleBooleanProperty active;
    private  SimpleStringProperty user_banned;
    private  SimpleStringProperty updated_at;
    private User user;

    public UserRow(User user){
        this.user = user;
        this.id = new SimpleStringProperty("<id>");
        this.level = new SimpleStringProperty("<level>");
        this.name = new SimpleStringProperty("<name>");
        this.email = new SimpleStringProperty("<email>");
        this.user_ip = new SimpleStringProperty("<user_ip>");
        this.active = new SimpleBooleanProperty(false);
        this.mod_title = new SimpleStringProperty("<mod_title>");
        this.user_banned = new SimpleStringProperty("<user_banned>");
        this.updated_at = new SimpleStringProperty("<updated_at>");



        this.id.set(String.valueOf(user.getId()));
        this.level.set(user.getLevel() == 80085 ? "Admin" : user.getLevel() == 69 ? "Moderator" : "User");
        this.name.set(user.getName());
        this.email.set(String.valueOf(user.getEmail()));
        this.user_ip.set(String.valueOf(user.getUser_ip()));
        this.active.set(user.getActive());
        this.mod_title.set(user.getMod_title());
        this.user_banned.set(String.valueOf(user.isUser_banned()));
        this.updated_at.set(user.getUpdated_at());

    }

    public String getMod_title() {
        return mod_title.get();
    }

    public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public SimpleStringProperty mod_titleProperty() {
        return mod_title;
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public String getLevel() {
        return level.get();
    }

    public SimpleStringProperty levelProperty() {
        return level;
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public String getUser_ip() {
        return user_ip.get();
    }

    public SimpleStringProperty user_ipProperty() {
        return user_ip;
    }


    public String getUser_banned() {
        return user_banned.get();
    }

    public SimpleStringProperty user_bannedProperty() {
        return user_banned;
    }

    public String getUpdated_at() {
        return updated_at.get();
    }

    public SimpleStringProperty updated_atProperty() {
        return updated_at;
    }

    public User getUser() {
        return user;
    }
}
