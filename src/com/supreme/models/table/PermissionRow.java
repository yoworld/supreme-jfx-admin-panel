package com.supreme.models.table;

import com.supreme.models.Permission;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PermissionRow {
    SimpleIntegerProperty user_id;
    SimpleStringProperty mod_hash;
    SimpleStringProperty user_name;
    SimpleStringProperty codename;
    SimpleBooleanProperty permission;
    SimpleStringProperty message;
    SimpleStringProperty status;
    SimpleStringProperty created_at;
    SimpleStringProperty updated_at;
    Permission permissionModel;

    public String getUser_name() {
        return user_name.get();
    }

    public SimpleStringProperty user_nameProperty() {
        return user_name;
    }

    public String getCodename() {
        return codename.get();
    }

    public SimpleStringProperty codenameProperty() {
        return codename;
    }

    public PermissionRow(Permission permissionModel){
        this.permissionModel = permissionModel;
        this.user_id = new SimpleIntegerProperty();
        this.mod_hash = new SimpleStringProperty();
        this.permission = new SimpleBooleanProperty();
        this.message = new SimpleStringProperty();
        this.created_at = new SimpleStringProperty();
        this.updated_at = new SimpleStringProperty();
        this.codename = new SimpleStringProperty();
        this.user_name = new SimpleStringProperty();
        this.status = new SimpleStringProperty();


        this.codename.set(permissionModel.getCodename());
        this.user_name.set(permissionModel.getUser_name());
        this.user_id.set(permissionModel.getUser_id());
        this.mod_hash.set(permissionModel.getMod_hash());
        this.permission.set(permissionModel.isPermission());
        this.status.set(permissionModel.getStatus());
        this.message.set(permissionModel.getMessage());
        this.created_at.set(permissionModel.getCreated_at());
        this.updated_at.set(permissionModel.getUpdated_at());
    }

    public Permission getPermissionModel() {
        return permissionModel;
    }

    public int getUser_id() {
        return user_id.get();
    }

    public SimpleIntegerProperty user_idProperty() {
        return user_id;
    }

    public String getMod_hash() {
        return mod_hash.get();
    }

    public SimpleStringProperty mod_hashProperty() {
        return mod_hash;
    }

    public boolean isPermission() {
        return permission.get();
    }

    public SimpleBooleanProperty permissionProperty() {
        return permission;
    }

    public String getMessage() {
        return message.get();
    }

    public SimpleStringProperty messageProperty() {
        return message;
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public String getCreated_at() {
        return created_at.get();
    }

    public SimpleStringProperty created_atProperty() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at.get();
    }

    public SimpleStringProperty updated_atProperty() {
        return updated_at;
    }
}
