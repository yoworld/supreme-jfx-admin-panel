package com.supreme.models.table;

import com.supreme.models.YoBuddy;
import javafx.beans.property.SimpleStringProperty;

public class BuddyRow {

    SimpleStringProperty buddy_uid;
    SimpleStringProperty buddy_name;
    SimpleStringProperty buddy_online;
    SimpleStringProperty buddy_status;
    SimpleStringProperty buddy_updated;
    SimpleStringProperty buddy_fb;
    private YoBuddy buddy;


    public BuddyRow(YoBuddy buddy) {
        this.buddy = buddy;

        this.buddy_uid = new SimpleStringProperty("<buddy_uid>");
        this.buddy_name = new SimpleStringProperty("<buddy_name>");
        this.buddy_online = new SimpleStringProperty("<buddy_online>");
        this.buddy_status = new SimpleStringProperty("<buddy_status>");
        this.buddy_updated = new SimpleStringProperty("<buddy_updated>");
        this.buddy_fb = new SimpleStringProperty("<buddy_fb>");


        this.buddy_uid.set(String.valueOf(buddy.getUser_id()));
        this.buddy_name.set(buddy.getUser_name());
        this.buddy_online.set(buddy.getUser_is_online() == 1 ? "true" : "false");
        this.buddy_status.set(getStatus(buddy.getUser_online_status()));
        this.buddy_updated.set(buddy.getUpdated_at());
        this.buddy_fb.set(String.valueOf(buddy.getUser_facebook_id()));
    }

    private String getStatus(int user_online_status) {
        switch (user_online_status) {
            case 1:
                return "Online";
            case 2:
                return "Away";
            case 3:
                return "Busy";
            case 4:
                return "Offline";
            default:
                return "Unknown";
        }
    }

    public String getBuddy_uid() {
        return buddy_uid.get();
    }

    public String getBuddy_name() {
        return buddy_name.get();
    }

    public String getBuddy_online() {
        return buddy_online.get();
    }

    public String getBuddy_status() {
        return buddy_status.get();
    }

    public String getBuddy_updated() {
        return buddy_updated.get();
    }

    public String getBuddy_fb() {
        return buddy_fb.get();
    }

    public YoBuddy getBuddy() {
        return buddy;
    }
}
