package com.supreme.models.table;

import com.supreme.models.Version;
import javafx.beans.property.SimpleStringProperty;

public class VersionRow {


    SimpleStringProperty codename;
    SimpleStringProperty version;
    SimpleStringProperty md5;
    SimpleStringProperty created_at;
    SimpleStringProperty publish;
    SimpleStringProperty tech_dig;
    private Version versionModel;


    public VersionRow(Version version) {
        this.versionModel = version;

        this.codename = new SimpleStringProperty("<codename>");
        this.version = new SimpleStringProperty("<version>");
        this.md5 = new SimpleStringProperty("<md5>");
        this.created_at = new SimpleStringProperty("<created_at>");
        this.publish = new SimpleStringProperty("<publish>");
        this.tech_dig = new SimpleStringProperty("<tech_dig>");


        this.publish.set(String.valueOf(version.isPublish()));
        this.codename.set(version.getCodename());
        this.version.set(version.getVersion());
        this.md5.set(version.getMd5());
        this.created_at.set(version.getCreated_at());
        this.tech_dig.set(version.getTech_dig());
    }

    public String getPublish() {
        return publish.get();
    }

    public String getCodename() {
        return codename.get();
    }

    public String getVersion() {
        return version.get();
    }

    public Version getVersionModel() {
        return versionModel;
    }

    public String getMd5() {
        return md5.get();
    }

    public String getCreated_at() {
        return created_at.get();
    }

    public String getTech_dig() {
        return tech_dig.get();
    }
}
