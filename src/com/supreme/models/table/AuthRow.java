package com.supreme.models.table;

import com.supreme.models.YoAuth;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AuthRow {

    private  SimpleStringProperty auth_active;
    private  SimpleStringProperty auth_level;
    private  SimpleStringProperty auth_uid;
    private  SimpleStringProperty auth_name;
    private  SimpleStringProperty auth_ip;
    private  SimpleStringProperty auth_banned;
    private  SimpleStringProperty auth_fbid;
    private  SimpleStringProperty fb_last_accessed;
    private SimpleIntegerProperty auth_assigned;
    private YoAuth auth;

    public int getAuth_assigned() {
        return auth_assigned.get();
    }

    public String getAuth_active() {
        return auth_active.get();
    }

    public String getAuth_uid() {
        return auth_uid.get();
    }

    public String getAuth_fbid() {
        return auth_fbid.get();
    }

    public String getAuth_banned() {
        return auth_banned.get();
    }

    public String getAuth_ip() {
        return auth_ip.get();
    }

    public String getAuth_name() {
        return auth_name.get();
    }

    public String getFb_last_accessed() {
        return fb_last_accessed.get();
    }

    public YoAuth getAuth() {
        return auth;
    }

    public String getAuth_level() {
        return auth_level.get();
    }


    public AuthRow(YoAuth auth){
        this.auth = auth;
        this.auth_uid = new SimpleStringProperty("<auth_uid>");
        this.auth_name = new SimpleStringProperty("<auth_name>");
        this.auth_ip = new SimpleStringProperty("<auth_ip>");
        this.auth_banned = new SimpleStringProperty("<auth_banned>");
        this.auth_fbid = new SimpleStringProperty("<auth_fbid>");
        this.fb_last_accessed = new SimpleStringProperty("<fb_last_accessed>");
        this.auth_level = new SimpleStringProperty("<auth_level>");
        this.auth_active = new SimpleStringProperty("<auth_active>");
        this.auth_assigned = new SimpleIntegerProperty();

        this.auth_assigned.set(auth.getAssigned_to());
        this.auth_uid.set(String.valueOf(auth.getUserId()));
        this.auth_name.set(String.format("%s - %s", auth.getUserFacebookName(), auth.getUserName()));
        this.auth_ip.set(auth.getUserIp());
        this.auth_banned.set(String.valueOf(auth.isUserBanned()));
        this.auth_active.set(String.valueOf(auth.isUserActive()));
        this.auth_fbid.set(String.valueOf(auth.getUserFacebookId()));
        this.fb_last_accessed.set(String.valueOf(auth.getUpdatedAt()));
        this.auth_level.set(auth.getUser_level() == 80085 ? "Admin" : auth.getUser_level() == 69 ? "Moderator" : "User");

    }

    public void setAuth(YoAuth auth) {
        this.auth = auth;
    }
}
