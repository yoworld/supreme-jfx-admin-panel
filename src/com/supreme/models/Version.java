package com.supreme.models;

public class Version {
    String md5;
    String codename;
    String version;
    String size;
    String type;
    String tech_dig;
    String created_at;
    String updated_at;
    boolean publish;


    public boolean isPublish() {
        return publish;
    }

    public String getMd5() {
        return md5;
    }

    public Version setMd5(String md5) {
        this.md5 = md5;
        return this;
    }

    public String getCodename() {
        return codename;
    }

    public Version setCodename(String codename) {
        this.codename = codename;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public Version setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getSize() {
        return size;
    }

    public Version setSize(String size) {
        this.size = size;
        return this;
    }

    public String getTech_dig() {
        return tech_dig;
    }

    public Version setTech_dig(String tech_dig) {
        this.tech_dig = tech_dig;
        return this;
    }

    public String getType() {
        return type;
    }

    public Version setType(String type) {
        this.type = type;
        return this;
    }

    public String getCreated_at() {
        return created_at;
    }

    public Version setCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public Version setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
        return this;
    }
}
