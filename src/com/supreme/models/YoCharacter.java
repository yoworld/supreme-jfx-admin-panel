package com.supreme.models;

public class YoCharacter {
    int user_id;
    String user_name;
    int user_gender;
    int user_badge_id;
    String user_last_login;
    String user_created_on;
    String first_time;
    String created_at;
    String updated_at;
    int logged_by_id;
    String logged_by_name;
    int user_model_level;
    String user_avatar;
    String user_network_id;
    String user_facebook_id;
    String user_type;
    String user_facebook_page;


    public int getUserId() {
        return user_id;
    }

    public String getUserName() {
        return user_name;
    }

    public int getUserGender() {
        return user_gender;
    }

    public int getUserBadgeId() {
        return user_badge_id;
    }

    public String getUserLastLogin() {
        return user_last_login;
    }

    public String getUserCreatedOn() {
        return user_created_on;
    }

    public String getFirstTime() {
        return first_time;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public String getUpdatedAt() {
        return updated_at;
    }

    public int getLoggedById() {
        return logged_by_id;
    }

    public String getLoggedByName() {
        return logged_by_name;
    }

    public int getUserModelLevel() {
        return user_model_level;
    }

    public String getUserAvatar() {
        return user_avatar;
    }

    public String getUserNetworkId() {
        return user_network_id;
    }

    public String getUserFacebookId() {
        return user_facebook_id;
    }

    public String getUserType() {
        return user_type;
    }

    public String getUserFacebookPage() {
        return user_facebook_page;
    }
}
