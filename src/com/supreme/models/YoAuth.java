package com.supreme.models;

public class YoAuth {
    int user_id;
    String user_name;
    boolean user_banned;
    boolean user_active;
    String user_ip;
    String created_at;
    String updated_at;
    long user_facebook_id;
    String api_token;
    String user_snapi_auth;
    String user_network_credentials;
    String user_yo_auth_key;
    String user_lk;
    String user_facebook_name;
    String user_secret;
    String user_game_window;
    String user_key;
    int user_level;
    int assigned_to;
    YoSettings user_settings;


    public int getAssigned_to() {
        return assigned_to;
    }

    public String getApi_token() {
        return api_token;
    }

    public YoSettings getUserSettings() {
        return user_settings;
    }

    public static class Builder {
        int user_id;
        String user_name;
        String snapi_auth;
        String fb_id;
        String network_credentials;
        String lk;
        String fb_name;
        Builder(){ }

        public static Builder create(){
            return new Builder();
        }

        public Builder setUser_id(int user_id) {
            this.user_id = user_id;
            return this;
        }

        public Builder setUser_name(String user_name) {
            this.user_name = user_name;
            return this;
        }

        public Builder setSnapi_auth(String snapi_auth) {
            this.snapi_auth = snapi_auth;
            return this;
        }

        public Builder setFb_id(String fb_id) {
            this.fb_id = fb_id;
            return this;
        }

        public Builder setNetwork_credentials(String network_credentials) {
            this.network_credentials = network_credentials;
            return this;
        }

        public Builder setLk(String lk) {
            this.lk = lk;
            return this;
        }

        public Builder setFb_name(String fb_name) {
            this.fb_name = fb_name;
            return this;
        }
    }


    public String getUserKey() {
        return user_key;
    }

    public int getUser_level() {
        return user_level;
    }

    public int getUserId() {
        return user_id;
    }

    public String getUserName() {
        return user_name;
    }

    public boolean isUserBanned() {
        return user_banned;
    }

    public String getUserIp() {
        return user_ip;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public String getUpdatedAt() {
        return updated_at;
    }

    public long getUserFacebookId() {
        return user_facebook_id;
    }

    public String getUserSnapiAuth() {
        return user_snapi_auth;
    }

    public String getUserNetworkCredentials() {
        return user_network_credentials;
    }

    public String getUserYoAuthKey() {
        return user_yo_auth_key;
    }

    public String getUserLk() {
        return user_lk;
    }

    public boolean isUserActive() {
        return user_active;
    }

    public String getUserFacebookName() {
        return user_facebook_name;
    }

    public String getUserSecret() {
        return user_secret;
    }

    public String getUserGameWindow() {
        return user_game_window;
    }
}
