package com.supreme.models;

public class Permission {
    int user_id;
    String mod_hash;
    String codename;
    String user_name;
    boolean permission;
    String created_at;
    String updated_at;
    String message;
    String status;

    public String getCodename() {
        return codename;
    }

    public String getUser_name() {
        return user_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getMod_hash() {
        return mod_hash;
    }

    public boolean isPermission() {
        return permission;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
