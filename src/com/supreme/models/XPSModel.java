package com.supreme.models;

public class XPSModel {
    public String player;
    public String name;
    public String first_name;
    public String last_name;
    public String last_login;
    public String gender;
    public String xp_count;
    public String facebook_id;
    public int sp_status;
    public int visit_back;

}
