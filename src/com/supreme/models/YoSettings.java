package com.supreme.models;

import java.util.ArrayList;
import java.util.List;

public class YoSettings {
    List<Integer> ignore = new ArrayList<>();
    boolean copycat = false;
    boolean rainbow = false;
    boolean spoofer = false;
    boolean stalker = false;
    boolean canfreeze = false;
    boolean chaosmode = false;
    boolean canttt = false;
    boolean chatrepeater = false;
    int frequency = 0;
    int lfrequency = -1;
    int id = 0;

    public int getId() {
        return id;
    }

    public YoSettings setId(int id) {
        this.id = id;
        return this;
    }

    public List<Integer> getIgnore() {
        return ignore;
    }

    public YoSettings setIgnore(List<Integer> ignore) {
        this.ignore = ignore;
        return this;
    }

    public boolean isChatrepeater() {
        return chatrepeater;
    }

    public boolean isCanttt() {
        return canttt;
    }

    public YoSettings setCanttt(boolean canttt) {
        this.canttt = canttt;
        return this;
    }

    public YoSettings setChatrepeater(boolean chatrepeater) {
        this.chatrepeater = chatrepeater;
        return this;
    }

    public boolean isCopycat() {
        return copycat;
    }

    public YoSettings setCopycat(boolean copycat) {
        this.copycat = copycat;
        return this;
    }

    public boolean isRainbow() {
        return rainbow;
    }

    public YoSettings setRainbow(boolean rainbow) {
        this.rainbow = rainbow;
        return this;
    }

    public boolean isSpoofer() {
        return spoofer;
    }

    public YoSettings setSpoofer(boolean spoofer) {
        this.spoofer = spoofer;
        return this;
    }

    public boolean isStalker() {
        return stalker;
    }

    public YoSettings setStalker(boolean stalker) {
        this.stalker = stalker;
        return this;
    }

    public boolean isCanfreeze() {
        return canfreeze;
    }

    public YoSettings setCanfreeze(boolean canfreeze) {
        this.canfreeze = canfreeze;
        return this;
    }

    public boolean isChaosmode() {
        return chaosmode;
    }

    public YoSettings setChaosmode(boolean chaosmode) {
        this.chaosmode = chaosmode;
        return this;
    }

    public int getFrequency() {
        return frequency;
    }

    public YoSettings setFrequency(int frequency) {
        this.frequency = frequency;
        return this;
    }

    public int getLfrequency() {
        return lfrequency;
    }

    public YoSettings setLfrequency(int lfrequency) {
        this.lfrequency = lfrequency;
        return this;
    }
}
